from astropy.convolution import Gaussian1DKernel

kernel = Gaussian1DKernel(stddev=1)
