.. _Select features from dataset:

****************************
Select features from dataset
****************************

Subset and/or reorder features in feature data X.

**Parameters**


:guilabel:`Dataset` [file]
    Dataset pickle file to select features from.


:guilabel:`Selected features` [string]
    Comma separated list of feature names or positions. E.g. use <code>1, 'Feature 2', 3</code> to select the first three features.

**Outputs**


:guilabel:`Output dataset` [fileDestination]
    Output destination pickle file.

