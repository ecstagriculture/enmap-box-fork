Processing Algorithms
*********************
    
.. toctree::
    :maxdepth: 1
       

    _auxilliary/index.rst
    _classification/index.rst
    _dataset_creation/index.rst
    _feature_selection/index.rst
    _post-processing/index.rst
    _raster_creation/index.rst
    _vector_creation/index.rst
    accuracy_assessment/index.rst
    auxilliary/index.rst
    classification/index.rst
    clustering/index.rst
    convolution__morphology_and_filtering/index.rst
    create_raster/index.rst
    create_sample/index.rst
    import_data/index.rst
    masking/index.rst
    post-processing/index.rst
    random/index.rst
    regression/index.rst
    resampling_and_subsetting/index.rst
    testdata/index.rst
    transformation/index.rst