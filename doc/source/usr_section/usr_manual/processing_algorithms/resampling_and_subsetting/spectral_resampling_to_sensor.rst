.. _Spectral Resampling to Sensor:

*****************************
Spectral Resampling to Sensor
*****************************

Spectrally resample a raster to a sensor.

**Parameters**


:guilabel:`Raster` [raster]
    Raster to be resampled.


:guilabel:`Sensor` [enum]
    Predefined target sensor


:guilabel:`Resampling Algorithm` [enum]
    undocumented parameter

    Default: *1*

**Outputs**


:guilabel:`Output Raster` [rasterDestination]
    Specify output path for raster.

