.. _Subset Raster Bands:

*******************
Subset Raster Bands
*******************

Subset raster bands by band numbers, band number ranges and wavelength ranges.

**Parameters**


:guilabel:`Raster` [raster]
    


:guilabel:`Band subset` [string]
    List of bands, band ranges or waveband ranges to subset. E.g. 1, 2, 4 6, 900. 1200. will select the first band, the second band, bands between 4 to 6, and wavebands between 900 to 1200 nanometers.


:guilabel:`Exclude Bad Bands` [boolean]
    Wether to exclude bad bands.

**Outputs**


:guilabel:`Output Raster` [rasterDestination]
    Specify output path.

