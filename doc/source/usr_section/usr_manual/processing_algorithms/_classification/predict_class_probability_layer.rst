.. _Predict class probability layer:

*******************************
Predict class probability layer
*******************************

Uses a fitted classifier to predict class probability layer from a raster layer with features.

**Parameters**


:guilabel:`Raster layer with features` [raster]
    A raster layer with bands used as features. Classifier features and raster bands are matched by name.


:guilabel:`Classifier` [file]
    A fitted classifier.


:guilabel:`Mask layer` [layer]
    A mask layer.

**Outputs**


:guilabel:`Output class probability layer` [rasterDestination]
    Output raster file destination.

