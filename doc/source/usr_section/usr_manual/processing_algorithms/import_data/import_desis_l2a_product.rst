.. _Import DESIS L2A Product:

************************
Import DESIS L2A Product
************************

Return VRT dataset with spectral information (wavelength and FWHM).

**Parameters**


:guilabel:`METADATA.xml` [file]
    Metadata file associated with L2A product.

**Outputs**


:guilabel:`Output VRT` [rasterDestination]
    Specify output path.

    Default: *DESIS_L2A_SPECTRAL.vrt*

