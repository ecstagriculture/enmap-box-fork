.. _Import PRISMA L2D Product:

*************************
Import PRISMA L2D Product
*************************

Return raster with correct interleave and spectral information (wavelength and FWHM).

**Parameters**


:guilabel:`HE5 File` [file]
    HE5 file associated with L2D product.

**Outputs**


:guilabel:`Output Raster` [rasterDestination]
    Specify output path.

    Default: *PRISMA_L2D_SPECTRAL.tif*

