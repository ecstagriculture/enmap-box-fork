.. _Import EnMAP L1B Product:

************************
Import EnMAP L1B Product
************************

Return VNIR and SWIR VRT datasets with spectral information (wavelength and FWHM) and data gains/offsets.

**Parameters**


:guilabel:`METADATA.XML` [file]
    Metadata file associated with L1B product.

**Outputs**


:guilabel:`Output VNIR VRT` [rasterDestination]
    Specify output path.

    Default: *EnMAP_L1B_VNIR.vrt*


:guilabel:`Output SWIR VRT` [rasterDestination]
    Specify output path.

    Default: *EnMAP_L1B_SWIR.vrt*

